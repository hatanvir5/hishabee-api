<?php

use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ExpenseCategoryController;
use App\Http\Controllers\ExpenseDetailsController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'v1'],function(){
    Route::post('all_category',  [ExpenseCategoryController::class, 'index'])->name('getAllCategory');
    Route::post('add_update_category',  [ExpenseCategoryController::class, 'createOrUpdateCategory'])->name('create_category');
    Route::post('delete_category',  [ExpenseCategoryController::class, 'deleteCategoy'])->name('deleteCategoy');
    Route::post('create_user',  [UserController::class, 'createUser'])->name('create_user');
    Route::get('get_user_list',  [UserController::class, 'index'])->name('get_user_list');
    Route::post('add_update_expense_item',  [ExpenseDetailsController::class, 'createOrUpdateExpense'])->name('create_expense_item');
    Route::post('delete_expense_item',  [ExpenseDetailsController::class, 'deleteExpenseItem'])->name('delete_expense_item');
    Route::post('all_expense_item',  [ExpenseDetailsController::class, 'index'])->name('all_expense_item');

});
