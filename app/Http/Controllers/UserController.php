<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;


class UserController extends Controller
{

    public function index(){
       $users =  User::all();
       if(count($users)>0){
            return  response()->json(['status'=> true,'res_code'=>'000','data'=>$users]);
       }
       return  response()->json(['status'=> false,'res_code'=>'401','data'=>[]]);

    }

    public function createUser(Request $request){
        return User::createUser($request);
    }
}
