<?php

namespace App\Http\Controllers;

use App\Models\ExpenseCategory;
use App\Http\Requests\StoreExpenseCategoryRequest;
use App\Http\Requests\UpdateExpenseCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExpenseCategoryController extends Controller
{

    public function index(Request $request)
    {
        $all_category = ExpenseCategory::getAllCategory($request);
        return  $all_category;
    }


    public function createOrUpdateCategory(Request $request)
    {
        $response = ExpenseCategory::crateUpdateCategory($request);
        return  $response;
    }

    public function deleteCategoy(Request $request)
    {
        $response = ExpenseCategory::deleteCategory($request);
        return  $response;
    }


}
