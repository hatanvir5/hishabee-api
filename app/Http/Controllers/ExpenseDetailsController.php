<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExpenseDetails;
class ExpenseDetailsController extends Controller
{
    public function index(Request $request)
    {
        $expense_items = ExpenseDetails::getAllExpenseItem($request);
        return  $expense_items;
    }


    public function createOrUpdateExpense(Request $request)
    {
        $response = ExpenseDetails::crateUpdateExpense($request);
        return  $response;
    }

    public function deleteExpenseItem(Request $request)
    {
        $response = ExpenseDetails::deleteExpenseItem($request);
        return  $response;
    }

}
