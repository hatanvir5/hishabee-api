<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use App\Models\FileAttachment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class ExpenseCategory extends Model
{
    use \Awobaz\Compoships\Compoships;

    protected $hidden = [
        'updated_at', 'created_at',
    ];

    public static function checkCommonValidation($request){

        $routeName = \Route::current()->getName();

        $validate_rules=[ 'user_id' => 'required'];

        if($routeName == 'create_category'){
            $validate_rule ='';
            if($request->category_id == null){
              $validate_rules=[
                'user_id' => 'required',
                'category_name' => 'required|unique:expense_categories,ct_name'
               ];
            }

        }elseif($routeName == 'deleteCategoy'){
            $validate_rules=[
                'user_id' => 'required',
                'category_id' => 'required'
            ];

        }

        $validator = Validator::make($request->all(),
            $validate_rules
        );

        $temp = $validator->errors()->all();
        if ($validator->fails()) {
            return response()->json(['status' =>false,'msg'=>$temp[0],'res_code' =>"401" ]);
        }

        $user = User::find($request->user_id);
        if(!$user){
            return response()->json(['status' =>false,'msg'=>'User Not Found','res_code' =>"401" ]);
        }
            return response()->json(['status' =>true,'res_code' =>"000" ]);

    }

    public static function commonQuery($request){
       return ['user_id'=>$request->user_id,'id'=>$request->category_id];
    }

    public static function getRecord($request){
        $query=self::commonQuery($request);
        $category=Self::where($query)->first();
        return $category;
    }


    public static function crateUpdateCategory($request){
        $responseData = [];
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }

        $status=true;
        $res_code = '000';
        $user_id = $request->user_id;
        DB::beginTransaction();

        try{
            if($request->category_id){
                $category=Self::getRecord($request);
                if(!$category){
                    return  response()->json(['status'=> false,'msg'=>'Expense Category Not Found','res_code'=>'401','data'=>[]]);
                }
                $message='Expense Category Successfully Updated.';
            }else{
                $category=new self();
                $message='Expense Category Successfully Created.';
                $category->created_at=Carbon::now();
            }
            $category->ct_name=$request->category_name;
            $category->user_id=$user_id;

            if($request->attachment){
                if( $category && $category->ct_image_path ){
                    $path = '/files/'.$category->ct_image_path;
                    Storage::disk('public')->delete($path);
                }

                $attachmentUpload=self::attachmentUpload($request);
                if( $attachmentUpload!=false){
                    $category->ct_image_path=$attachmentUpload;
                }else{
                    $category->ct_image_path=null;
                }
            }
            $category->updated_at=Carbon::now();
            $category->save();

        }catch(\QueryException  $e){
           $message=$e->getMessage();
           $status=false;
           $res_code = '401';
           DB::rollback();
        }
        DB::commit();

        return  response()->json(['status'=> $status,'msg'=>$message,'res_code'=>$res_code,'data'=>$category]);
    }


    public static function getAllCategory($request){

        $responseData = [];
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }



        $res_code = "500";
        $sortOrder=['created_at','desc'];


        $condition=[];
        if($request->filter_by_category){
            $condition = ['id'=>$request->filter_by_category];
        }
        $data= self::where('user_id',$request->user_id)->where($condition )->withSum('expenseDetails', 'cost_amount')->get();
        $totalCost = $data->sum('expense_details_sum_cost_amount');


        if(count($data) > 0){
            if($request->sortType == 'by_time_old_to_new'){
                $sortOrder=['created_at','asc'];
            }elseif($request->sortType == 'by_cost_low_to_high'){
                $sortOrder=['expense_details_sum_cost_amount','asc'];
            }elseif($request->sortType == 'by_cost_high_to_low'){
                $sortOrder=['expense_details_sum_cost_amount','dsc'];
            }

            $sorted = $data->sortBy([
                $sortOrder
            ]);

            if($request->take_item){
                $sorted= $sorted->take($request->take_item);
            }

            $sorted->values()->all();
            $data = $sorted;

            foreach($data as &$item){
                $category_cost = $item->expense_details_sum_cost_amount;
                $percentage = '0 %';
                if($totalCost > 0){
                    $percentage =   ($category_cost / $totalCost)*100;
                }
                $item->percentage =  round($percentage,2).' %';
            }
            $responseData['total_cost'] = $totalCost;
            $responseData['category_data'] = $data->toArray();
        }else{
            return response()->json(['status' =>false,'msg'=>'No record found','Data' =>  $responseData,'res_code' =>"401" ]);
        }

        if(count($data) > 0 ){
            $res_code = "000";
        }
        return  response()->json(['status'=>true ,'res_code'=>$res_code,'data'=>$responseData]);
    }



    public static function attachmentUpload($request){
        $file=$request->attachment;

        $allowedMimeTypes = ['image/jpeg','image/gif','image/png'];
        $contentType = $request->attachment->getClientMimeType();
        if(! in_array($contentType, $allowedMimeTypes) ){
          $image=false;
        }else{
            $image=true;
        }
        if($file){
            $file_size= $file->getSize();
            $file_path="/files/";
            if($request->file_path){
                $file_path= $request->file_path;
            }

            $original_name=explode('.', $file->getClientOriginalName())[0];
            $uniqueFileName = $original_name .'_'.uniqid(). '.' . $file->getClientOriginalExtension();
            if($image){
                $status= \Storage::disk('public')->put( $file_path . $uniqueFileName, \File::get($file));
            }else{
                $status= \Storage::disk('local')->put(  $file_path . $uniqueFileName, \File::get($file));
            }

            return   $uniqueFileName;
        }else{

            return false;
        }
    }


    public function deleteExpenseList(){
        $this->expenseDetails()->delete();
        return parent::delete();
    }

    public static function deleteCategory($request){
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }
        DB::beginTransaction();
        try{
            $status=true;
            $res_code = '000';
            $message='Category Successfully Deleted.';
            $category=Self::getRecord($request);
            if( $category){
                $category->deleteExpenseList();
            }else{
                return  response()->json(['status'=> false,'msg'=>'Category Not Found.','res_code'=>'401']);
            }

        }catch(\QueryException  $e){
           $message=$e->getMessage();
           $status=false;
           $res_code = '401';
           DB::rollback();
        }
        DB::commit();
        return  response()->json(['status'=> $status,'msg'=>$message,'res_code'=>$res_code]);

    }

    public function expenseDetails()
    {
        return $this->hasMany(ExpenseDetails::class, ['category_id','user_id'], ['id','user_id']);
    }
}
