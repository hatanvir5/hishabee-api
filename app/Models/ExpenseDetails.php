<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use App\Models\FileAttachment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ExpenseDetails extends Model
{
    use \Awobaz\Compoships\Compoships;
    protected $hidden = [
        'updated_at', 'created_at',
    ];

    public static function checkCommonValidation($request){
        $responseData = [];
        $routeName = \Route::current()->getName();

        $validate_rules=[ 'user_id' => 'required'];

        if($routeName == 'create_expense_item'){

            $validate_rules=[
                'user_id' => 'required',
                'category_id' => 'required',
                'cost_amount' => 'required|numeric',
                'expense_date' => 'required|date'
            ];

        }elseif($routeName == 'delete_expense_item'){
            $validate_rules=[
                'user_id' => 'required',
                'category_id' => 'required',
                'expense_item_id' => 'required'
            ];
        }

        $validator = Validator::make($request->all(),
            $validate_rules
        );

        $temp = $validator->errors()->all();
        if ($validator->fails()) {
            return response()->json(['status' =>false,'msg'=>$temp[0],'res_code' =>"401" ]);
        }

        $user = User::find($request->user_id);
        if(!$user){
            return response()->json(['status' =>false,'msg'=>'User Not Found','res_code' =>"401" ]);
        }

        if( $routeName == 'create_expense_item'){
            $expenseCategory = ExpenseCategory::where(['user_id'=>$request->user_id,'id'=>$request->category_id])->first();
            if(!$expenseCategory){
                return response()->json(['status' =>false,'msg'=>'Category Not Found For Selected User','res_code' =>"401" ]);
            }
        }


        return response()->json(['status' =>true,'res_code' =>"000" ]);

    }

    public static function commonQuery($request){
       return ['user_id'=>$request->user_id,'id'=>$request->expense_item_id,'category_id'=>$request->category_id,];
    }

    public static function getRecord($request){
        $query=self::commonQuery($request);
        $expenseItem=Self::where($query)->first();
        return $expenseItem;
    }


    public static function crateUpdateExpense($request){
        $responseData = [];
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }
        $status=true;
        $res_code = '000';
        $user_id = $request->user_id;
        DB::beginTransaction();

        try{
            if($request->expense_item_id){
                $expenseItem=Self::getRecord($request);
                if(!$expenseItem){
                    return  response()->json(['status'=> false,'msg'=>'Expense Item Not Found','res_code'=>'401','data'=>[]]);
                }
                $message='Expense item Successfully Updated.';
            }else{
                $expenseItem=new self();
                $message='Expense item Successfully Created.';
                $expenseItem->created_at=Carbon::now();
            }
            $expenseItem->user_id=$request->user_id;
            $expenseItem->category_id=$request->category_id;
            $expenseItem->cost_amount=$request->cost_amount;
            $expenseItem->cost_via=$request->cost_via;
            $expenseItem->description=$request->description;
            $expenseItem->expense_date=$request->expense_date;

            if($request->attachment){
                if( $expenseItem && $expenseItem->attachment_path ){
                    $path = '/files/'.$expenseItem->attachment_path;
                    Storage::disk('public')->delete($path);
                }

                $attachmentUpload=self::attachmentUpload($request);
                if( $attachmentUpload!=false){
                    $expenseItem->attachment_path=$attachmentUpload;
                }else{
                    $expenseItem->attachment_path=null;
                }
            }

            $expenseItem->updated_at=Carbon::now();
            $expenseItem->save();

        }catch(\QueryException  $e){
           $message=$e->getMessage();
           $status=false;
           $res_code = '401';
           DB::rollback();
        }
        DB::commit();

        return  response()->json(['status'=> $status,'msg'=>$message,'res_code'=>$res_code,'data'=>$expenseItem]);
    }


    public static function getAllExpenseItem($request){

        $responseData = [];
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }

        $res_code = "500";
        $sortOrder=['expense_date','desc'];


        $condition=[];
        if($request->filter_by_category){
            $condition = ['category_id'=>$request->filter_by_category];
        }

        $date_filter=false;
        if($request->filter_start_date || $request->filter_end_date ){
            $date_filter=true;
            if($request->filter_start_date){
                $start_date = $request->filter_start_date;
            }else{
                $start_date = $request->filter_end_date;
            }

            if($request->filter_end_date){
                $end_date = $request->filter_end_date;
            }else{
                $end_date = $request->filter_start_date;
            }
        }

        if($request->search){
            $search = $request->search;
            $query= self::where('user_id',$request->user_id)->where($condition)->with('expenseCategory:id,ct_name')
            ->where(function ($q) use ($search) {
                $q->orWhere('cost_amount', 'LIKE', "%{$search}%");
                $q->orWhere('cost_via', 'LIKE', "%{$search}%");
                $q->orWhere('description', 'LIKE', "%{$search}%");
                $q->orWhere('expense_date', 'LIKE', "%{$search}%");
                $q->orWhereHas('expenseCategory', function ($q) use ($search) {
                    $q->where('ct_name', 'like', $search . '%');
                });
            });
            if($date_filter){
                $query =   $query->whereBetween('expense_date', [$start_date,$end_date]);
            }
            $data = $query->get();
        }else{
            $query= self::where('user_id',$request->user_id)->where($condition)->with('expenseCategory:id,ct_name');
            if($date_filter){
                $query = $query->whereBetween('expense_date', [$start_date,$end_date]);
            }
            $data = $query->get();
        }

        $totalCost = $data->sum('cost_amount');

        if(count($data) > 0){
            if($request->sortType == 'by_time_old_to_new'){
                $sortOrder=['expense_date','asc'];
            }elseif($request->sortType == 'by_cost_low_to_high'){
                $sortOrder=['cost_amount','asc'];
            }elseif($request->sortType == 'by_cost_high_to_low'){
                $sortOrder=['cost_amount','dsc'];
            }

            $sorted = $data->sortBy([
                $sortOrder
            ]);

            if($request->take_item){
                $sorted= $sorted->take($request->take_item);
            }

            $sorted->values()->all();
            $data = $sorted;
            $responseData['total_cost'] = $totalCost;
            $responseData['expanse_item'] = $data->toArray();
        }else{
            return response()->json(['status' =>false,'msg'=>'No record found','Data' =>  $responseData,'res_code' =>"401" ]);
        }

        if(count($data) > 0 ){
            $res_code = "000";
        }
        return  response()->json(['status'=>true ,'res_code'=>$res_code,'data'=>$responseData]);
    }



    public static function attachmentUpload($request){
        $file=$request->attachment;

        $allowedMimeTypes = ['image/jpeg','image/gif','image/png'];
        $contentType = $request->attachment->getClientMimeType();
        if(! in_array($contentType, $allowedMimeTypes) ){
          $image=false;
        }else{
            $image=true;
        }
        if($file){
            $file_size= $file->getSize();
            $file_path="/files/";
            if($request->file_path){
                $file_path= $request->file_path;
            }

            $original_name=explode('.', $file->getClientOriginalName())[0];
            $uniqueFileName = $original_name .'_'.uniqid(). '.' . $file->getClientOriginalExtension();
            if($image){
                $status= \Storage::disk('public')->put( $file_path . $uniqueFileName, \File::get($file));
            }else{
                $status= \Storage::disk('local')->put(  $file_path . $uniqueFileName, \File::get($file));
            }

            return   $uniqueFileName;
        }else{

            return false;
        }
    }

    public static function deleteExpenseItem($request){
        $validateCheck = self::checkCommonValidation($request);
        if($validateCheck->getData()->status == false){
            return  $validateCheck;
        }
        DB::beginTransaction();
        try{
            $status=true;
            $res_code = '000';
            $message='Expense Item Successfully Deleted.';
            $expenseItem=Self::getRecord($request);

            if( $expenseItem){
                $expenseItem->delete();
            }else{
                return  response()->json(['status'=> false,'msg'=>'Expense Item Not Found.','res_code'=>'401']);
            }

        }catch(\QueryException  $e){
           $message=$e->getMessage();
           $status=false;
           $res_code = '401';
           DB::rollback();
        }
        DB::commit();
        return  response()->json(['status'=> $status,'msg'=>$message,'res_code'=>$res_code]);

    }

    public function expenseCategory()
    {
        return $this->belongsTo(ExpenseCategory::class, 'category_id');
    }

}
