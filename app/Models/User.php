<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];





    public static function createUser($request){
        $validate_rules=[ 'phone' => 'required|unique:users,phone|numeric|digits:11','name' => 'required|string| min:1','password' => 'required| min:6','address'=>'required|string| min:1'];
        $validator = Validator::make($request->all(),
            $validate_rules
        );
        $temp = $validator->errors()->all();
        if ($validator->fails()) {
            return response()->json(['status' =>false,'msg'=>$temp[0],'res_code' =>"401" ]);
        }

        try{

        $status=true;
        $res_code = '000';
        $message= 'User Successfully Created.';
        $user = new self();
        $user->name= $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->password =  bcrypt( $request->password);
        $user->save();
        }catch(\Exception $e){
            $message=$e->getMessage();
            $status=false;
            $res_code = '401';
            DB::rollback();
        }
        DB::commit();
        return  response()->json(['status'=> $status,'msg'=>$message,'res_code'=>$res_code,'data'=>$user]);


    }
}
