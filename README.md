*****API Task *******

--  This is done as required.
-- ### Version Requisition

- PHP Version- 7.4*

### How to install this application?

1. Configure .env file with your database name
2. run php artisan migrate


For the database, you can also import from hishabee_api.sql which exists in the database folder.
Postman json collection from  postman-collection folder 

Run - php artisan serve
